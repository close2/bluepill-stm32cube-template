# template for stm32 hal projects

This project uses a black magic probe (bluepill with bmp firmware).

`git clone https://github.com/ObKo/stm32-cmake.git` somewhere and set location in `CMakeLists.txt`

Download stm32cube (not stm32cubemx) and set location in `CMakeLists.txt`.

Then

* `make cmake-debug`
* `make`
* `make flash`


The flash command assumes that you have added udev rules for the black magic probe:

`/etc/udev/rules.d/99-blackmagic.rules`:

```
# Black Magic Probe
# there are two connections, one for GDB and one for uart debugging
  SUBSYSTEM=="tty", ATTRS{interface}=="Black Magic GDB Server", SYMLINK+="ttyBmpGdb"
  SUBSYSTEM=="tty", ATTRS{interface}=="Black Magic UART Port", SYMLINK+="ttyBmpTarg"

# License

my code is MIT.  Some files are copied and might have a different license (see header in files).
AFAIK everything is open source.

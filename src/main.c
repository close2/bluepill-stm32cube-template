#include <stm32f1xx_hal.h>

void initGPIO()
{
    GPIO_InitTypeDef GPIO_Config;

    GPIO_Config.Mode = GPIO_MODE_AF_PP;
    GPIO_Config.Pull = GPIO_NOPULL;
    GPIO_Config.Speed = GPIO_SPEED_FREQ_LOW;

    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_AFIO_CLK_ENABLE();

    GPIO_Config.Pin = GPIO_PIN_7;
    HAL_GPIO_Init(GPIOB, &GPIO_Config);
}

void initTimers()
{
    TIM_HandleTypeDef TIM_Handle;

    // 10 kHz timer.
    __HAL_RCC_TIM4_CLK_ENABLE();
    TIM_Handle.Instance = TIM4;
    TIM_Handle.Init.Prescaler = (uint16_t)(HAL_RCC_GetPCLK1Freq() / 5000) - 1;

    // 1 Hz blinking
    TIM_Handle.Init.Period = 10000;
    TIM_Handle.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    TIM_Handle.Init.CounterMode = TIM_COUNTERMODE_UP;

    HAL_TIM_Base_Init(&TIM_Handle);
    HAL_TIM_PWM_Init(&TIM_Handle);

    TIM_OC_InitTypeDef TIM_OCConfig;

    TIM_OCConfig.OCMode = TIM_OCMODE_PWM1;
    // 5000 / 10000 = 50% duty cycle.
    TIM_OCConfig.Pulse = 4999;
    TIM_OCConfig.OCPolarity = TIM_OCPOLARITY_HIGH;
    TIM_OCConfig.OCFastMode = TIM_OCFAST_DISABLE;

    HAL_TIM_PWM_ConfigChannel(&TIM_Handle, &TIM_OCConfig, TIM_CHANNEL_2);
    HAL_TIM_PWM_Start(&TIM_Handle, TIM_CHANNEL_2);
}

static void initClock(void)
{
    RCC_ClkInitTypeDef RCC_ClkInitStruct;
    RCC_OscInitTypeDef RCC_OscInitStruct;

    __HAL_RCC_PWR_CLK_ENABLE();

    uint8_t fLatency;

    RCC_OscInitStruct.OscillatorType  = RCC_OSCILLATORTYPE_HSE;
    RCC_OscInitStruct.HSEState = RCC_HSE_ON;
    RCC_OscInitStruct.LSEState = RCC_LSE_OFF;
    RCC_OscInitStruct.HSIState = RCC_HSI_OFF;
    RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;

    // 8 MHz * 9 = 72 MHz SYSCLK
    RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
    RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
    fLatency = FLASH_LATENCY_2;

    HAL_RCC_OscConfig(&RCC_OscInitStruct);

    RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

    HAL_RCC_ClockConfig(&RCC_ClkInitStruct, fLatency);
}

void initAll(void)
{
    HAL_Init();

    initClock();
    initGPIO();
    initTimers();
}

int main(void)
{
    initAll();
    for (;;);
    return 0;
}
